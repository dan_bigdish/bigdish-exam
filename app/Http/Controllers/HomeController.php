<?php

namespace App\Http\Controllers;
use App\Notifications\UserNotification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth'=>'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user = auth()->user();

        if ($user['notified_at'] === null || $user['notified_at'] == '') 
        {
            $user->notify(new UserNotification($user));
        }

        return view('home');
    }
}
