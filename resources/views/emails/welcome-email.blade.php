@component('mail::message')

<h1>You Have Successfully Verified Your Account</h1>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
